#!/bin/bash

legacy_export_json_file=$1

if ! [ -f "$legacy_export_json_file" ]; then
    echo "Usage: $0 [legacy_export_json_file_path]"
    exit 1
fi

# handle relations
relations="labels milestones issues snippets releases project_members merge_requests ci_pipelines external_pull_requests auto_devops triggers pipeline_schedules container_expiration_policy services protected_branches protected_tags project_feature custom_attributes prometheus_metrics project_badges ci_cd_settings error_tracking_setting metrics_setting boards protected_environments service_desk_setting"

result_dir="./convert_result"
mkdir -p $result_dir
mkdir -p $result_dir/project

for relation in ${relations}
do
    target_file="${result_dir}/project/${relation}.ndjson"
    tmp_file="${target_file}_tmp"

    echo "generating ${target_file} ..."
    jq ".${relation}" ${legacy_export_json_file} > ${tmp_file}

    type=`jq 'type' ${tmp_file}`

    case $type in

        \"object\")
            jq -c '.' ${tmp_file} > ${target_file}
            rm ${tmp_file}
            ;;

        \"array\")
            jq -c '.[]' ${tmp_file} > ${target_file}
            rm ${tmp_file}
            ;;

        *)
            mv ${tmp_file} ${target_file}
            ;;
    esac

    # delete empty relation file
    if ! [ -s ${target_file} ]
    then
        echo "removing empty file: ${target_file}"
        rm ${target_file}
    fi

    # delete relation file which only has "null" as its content
    file_size=$(wc -c "${target_file}" | awk '{print $1}')
    if [ ${file_size} -eq 5 ]
    then
        file_content=$(cat ${target_file})
        if [ ${file_content} == "null" ]
        then
            echo "removing 'null' content file: ${target_file}"
            rm ${target_file}
        fi
    fi
done

# handle project root attributes
relations_comma_delimited=".labels, .milestones, .issues, .snippets, .releases, .project_members, .merge_requests, .ci_pipelines, .external_pull_requests, .auto_devops, .triggers, .pipeline_schedules, .container_expiration_policy, .services, .protected_branches, .protected_tags, .project_feature, .custom_attributes, .prometheus_metrics, .project_badges, .ci_cd_settings, .error_tracking_setting, .metrics_setting, .boards, .protected_environments, .service_desk_setting"

target_file="${result_dir}/project.json"

echo "generating ${target_file} ..."
jq "del(${relations_comma_delimited})" ${legacy_export_json_file} | jq -c '.' > ${target_file}


# below are how we get the list of `relations` and `root_attributes`

#[74] pry(main)> reader.project_tree[:include].map(&:each_key).map(&:first)
# [:labels,
# :milestones,
# :issues,
# :snippets,
# :releases,
# :project_members,
# :merge_requests,
# :ci_pipelines,
# :external_pull_requests,
# :auto_devops,
# :triggers,
# :pipeline_schedules,
# :container_expiration_policy,
# :services,
# :protected_branches,
# :protected_tags,
# :project_feature,
# :custom_attributes,
# :prometheus_metrics,
# :project_badges,
# :ci_cd_settings,
# :error_tracking_setting,
# :metrics_setting,
# :boards,
# :protected_environments,
# :service_desk_setting]
# 
# [85] pry(main)> project.as_json(options)
# => {"description"=>"Vim, Tmux and others",
# "visibility_level"=>20,
# "archived"=>false,
# "merge_requests_template"=>nil,
# "merge_requests_rebase_enabled"=>false,
# "approvals_before_merge"=>0,
# "reset_approvals_on_push"=>nil,
# "merge_requests_ff_only_enabled"=>false,
# "issues_template"=>nil,
# "shared_runners_enabled"=>true,
# "build_coverage_regex"=>nil,
# "build_allow_git_fetch"=>true,
# "build_timeout"=>3600,
# "pending_delete"=>false,
# "public_builds"=>true,
# "last_repository_check_failed"=>nil,
# "container_registry_enabled"=>true,
# "only_allow_merge_if_pipeline_succeeds"=>false,
# "has_external_issue_tracker"=>false,
# "request_access_enabled"=>true,
# "has_external_wiki"=>false,
# "ci_config_path"=>nil,
# "only_allow_merge_if_all_discussions_are_resolved"=>false,
# "repository_size_limit"=>nil,
# "printing_merge_request_link_enabled"=>true,
# "auto_cancel_pending_pipelines"=>"enabled",
# "service_desk_enabled"=>false,
# "delete_error"=>nil,
# "disable_overriding_approvers_per_merge_request"=>nil,
# "resolve_outdated_diff_discussions"=>false,
# "jobs_cache_index"=>nil,
# "external_authorization_classification_label"=>"",
# "pages_https_only"=>false,
# "external_webhook_token"=>"D3mVYFzZkgZ5kMfcW_wx",
# "merge_requests_author_approval"=>nil,
# "merge_requests_disable_committers_approval"=>nil,
# "require_password_to_approve"=>nil,
# "remove_source_branch_after_merge"=>true,
# "autoclose_referenced_issues"=>true,
# "suggestion_commit_message"=>nil}
